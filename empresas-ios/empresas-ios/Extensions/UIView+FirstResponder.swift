//
//  UIView+FirstResponder.swift
//  empresas-ios
//
//  Created by Caio Gomes on 21/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    var firstResponder: UIView? {
        guard !isFirstResponder else { return self }

        for subview in subviews {
            if let firstResponder = subview.firstResponder {
                return firstResponder
            }
        }

        return nil
    }
}
