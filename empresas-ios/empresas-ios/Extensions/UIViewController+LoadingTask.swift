//
//  UIViewController+LoadingTask.swift
//  empresas-ios
//
//  Created by Caio Gomes on 20/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func loadScreen() {
        // TODO: implement a dispatch to allow it only when the actual viewcontroller is presented in the screen
        let alert = UIAlertController(title: nil, message: "", preferredStyle: .alert)
        
        // Search for the contentView in the popup and remove the background
        if let rootView = alert.view {
            var frontier = [rootView]
            while !frontier.isEmpty {
                let view = frontier.popLast()
                if let v = view as? UIVisualEffectView {
                    v.effect = .none
                    v.contentView.backgroundColor = .none
                    break
                }
                for child in view?.subviews ?? [] {
                    frontier.append(child)
                }
            }
        }

//        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//        loadingIndicator.hidesWhenStopped = true
//        loadingIndicator.style = UIActivityIndicatorView.Style.medium
//        loadingIndicator.startAnimating()
        let loadingIndicator = CustomLoadView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        let w1 = loadingIndicator.widthAnchor.constraint(equalToConstant: 100)
        let h1 = loadingIndicator.heightAnchor.constraint(equalToConstant: 100)
        let midx = NSLayoutConstraint(item: loadingIndicator, attribute: .centerX, relatedBy: .equal, toItem: alert.view, attribute: .centerX, multiplier: 1, constant: 0)
        let midy = NSLayoutConstraint(item: loadingIndicator, attribute: .centerY, relatedBy: .equal, toItem: alert.view, attribute: .centerY, multiplier: 1, constant: 0)
        
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        alert.view.translatesAutoresizingMaskIntoConstraints = false

        // TODO: change to the custom loading widget
        alert.view.addSubview(loadingIndicator)
        NSLayoutConstraint.activate([w1, h1, midx, midy])
        
        present(alert, animated: true) {
            loadingIndicator.setAnimation()
        }
    }
    
    func dismissLoad(_ handler: @escaping ()->Void = {}) {
        if !self.isBeingPresented {
            dismiss(animated: false, completion: handler)
        }
    }
}
