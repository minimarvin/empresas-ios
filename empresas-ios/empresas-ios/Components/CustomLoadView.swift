//
//  CustomLoadView.swift
//  empresas-ios
//
//  Created by Caio Gomes on 21/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import UIKit

class CustomLoadView: UIView {
    private let innerImage = UIImageView(image: #imageLiteral(resourceName: "innerImage"))
    private let outerImage = UIImageView(image: #imageLiteral(resourceName: "outerImage"))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupImages()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupImages()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setAnimation()
    }
    
    private func setupImages() {
        self.addSubview(innerImage)
        self.addSubview(outerImage)
        
        innerImage.translatesAutoresizingMaskIntoConstraints = false
        outerImage.translatesAutoresizingMaskIntoConstraints = false
        
        let midX1 = NSLayoutConstraint(item: innerImage, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let midY1 = NSLayoutConstraint(item: innerImage, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        let w1 = innerImage.widthAnchor.constraint(equalToConstant: 47)
        let h1 = innerImage.heightAnchor.constraint(equalToConstant: 47)

        let midX2 = NSLayoutConstraint(item: outerImage, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let midY2 = NSLayoutConstraint(item: outerImage, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        let w2 = outerImage.widthAnchor.constraint(equalToConstant: 72)
        let h2 = outerImage.heightAnchor.constraint(equalToConstant: 72)

        self.addConstraints([midX1, midX2, midY1, midY2])
        NSLayoutConstraint.activate([
            midX1,
            midY1,
            w1,
            h1,
            midX2,
            midY2,
            w2,
            h2
        ])
    }
    
    func setAnimation() {
        innerImage.layer.removeAllAnimations()
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = -Double.pi * 2 //Minus can be Direction
        rotationAnimation.duration = 2
        rotationAnimation.repeatCount = .infinity
        innerImage.layer.add(rotationAnimation, forKey: nil)
        
        outerImage.layer.removeAllAnimations()
        let backwardRotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        backwardRotationAnimation.fromValue = 0.0
        backwardRotationAnimation.toValue = Double.pi * 2 //Minus can be Direction
        backwardRotationAnimation.duration = 2
        backwardRotationAnimation.repeatCount = .infinity
        outerImage.layer.add(backwardRotationAnimation, forKey: nil)
        
    }
}
