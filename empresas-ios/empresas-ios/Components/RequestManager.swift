//
//  RequestManager.swift
//  empresas-ios
//
//  Created by Caio Gomes on 20/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import Foundation

class RequestManager {
    var headers: [String:String]=["Content-Type":"application/json"]
    
    func signIn(email: String, password: String, callback: ((Data?, HTTPURLResponse?, Error?) -> Void)?=nil) {
        let session = URLSession.shared
        let url = URL(string: "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in")
        
        var request = URLRequest(url: url!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 12000)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let json = [
            "email": email,
            "password": password
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])

        let task = session.uploadTask(with: request, from: jsonData) { data, response, error in
            let httpResponse = response as! HTTPURLResponse
            if httpResponse.statusCode == 200 {
                let accessToken = httpResponse.value(forHTTPHeaderField: "access-token")
                let client = httpResponse.value(forHTTPHeaderField: "client")
                let uid = httpResponse.value(forHTTPHeaderField: "uid")
                
                self.headers["access-token"] = accessToken
                self.headers["client"] = client
                self.headers["uid"] = uid
                
                print(self.headers)
            }
            
            DispatchQueue.main.async {
                callback?(data, httpResponse, error)
            }
        }
        
        task.resume()
    }
    
    func search(prefix: String, callback: ((Data?, HTTPURLResponse?, Error?) -> Void)?=nil) {
        let session = URLSession.shared
        // TODO: verify the anatomy of the request to make the request better
//        let url = URL(string: "https://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types=1&name=\(prefix)")
        let url = URL(string: "https://empresas.ioasys.com.br/api/v1/enterprises?name=\(prefix)")
        var request = URLRequest(url: url!, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 12000)
        request.httpMethod = "GET"
        for (field, value) in headers {
            request.setValue(value, forHTTPHeaderField: field)
        }
        
        let task = session.dataTask(with: request) { (data, response, error) in
            let httpResponse = response as! HTTPURLResponse
            
            DispatchQueue.main.async {
                callback?(data, httpResponse, error)
            }
        }
        
        task.resume()
    }
}

