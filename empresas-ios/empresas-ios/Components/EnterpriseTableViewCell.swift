//
//  EnterpriseTableViewCell.swift
//  empresas-ios
//
//  Created by Caio Gomes on 21/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import UIKit

class EnterpriseTableViewCell: UITableViewCell {

    @IBOutlet private weak var enterpriseImage: UIImageView!
    @IBOutlet private weak var enterpriseLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    private var _enterpriseColor: UIColor = .purple
    var enterpriseColor: UIColor {
        get {
            return _enterpriseColor
        }
        set {
            _enterpriseColor = newValue
            bgView.backgroundColor = _enterpriseColor
        }
    }
    
    private var _enterprise: Enterprise? = nil
    var enterprise: Enterprise? {
        get {
            return self._enterprise
        }
        set {
            self._enterprise = newValue
            if let enterprise = self._enterprise {
                self.enterpriseLabel.text = enterprise.enterprise_name
                // TODO: implement the image loading
//                self.enterpriseImage.downloaded(from: "https://s2.glbimg.com/z_gIOSUdsxyNGClgVLYVBHBziyw=/0x0:400x400/400x400/s.glbimg.com/po/tt2/f/original/2016/05/20/new-google-favicon-logo.png")
                if let url = enterprise.photo {
                    self.enterpriseImage.downloaded(from: "https://empresas.ioasys.com.br"+url)
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.bgView.layer.cornerRadius = 4
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
