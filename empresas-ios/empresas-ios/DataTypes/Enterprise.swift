//
//  SearchData.swift
//  empresas-ios
//
//  Created by Caio Gomes on 21/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import Foundation

class Enterprises: Codable {
    var enterprises: [Enterprise]
}

class Enterprise: Codable {
    var city: String=""
    var country: String=""
    var description: String=""
    var email_enterprise: String?=nil
    var enterprise_name: String=""
    var enterprise_type: EnterpriseType=EnterpriseType()
    var facebook: String?=nil
    var id: Int=0
    var linkedin: String?=nil
    var own_enterprise: Bool=false
    var phone: String?=nil
    var photo: String?=nil
    var sharePrice: Int?=nil
    var twitter: String?=nil
    var value: Int=0
}

class EnterpriseType: Codable {
    var enterprise_type_name: String=""
    var id: Int=0
}
