//
//  SearchViewController.swift
//  empresas-ios
//
//  Created by Caio Gomes on 20/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var topArea: UIView!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var topImageIcons: UIImageView!
    @IBOutlet weak var searchInput: TextField!
    @IBOutlet weak var contentArea: UIView!
    @IBOutlet weak var topAreaPosition: NSLayoutConstraint!
    @IBOutlet weak var loadingView: CustomLoadView!
    @IBOutlet weak var noResultLabel: UILabel!
    @IBOutlet weak var resultsView: UIView!
    @IBOutlet weak var resultsLabel: UILabel!
    @IBOutlet weak var resultsTableView: UITableView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    
    
    var requestManager = RequestManager()
    
    var _enterprises: [Enterprise] = []
    var enterprises: [Enterprise] {
        get {
            return _enterprises
        }
        set {
            _enterprises = newValue
            self.resultsLabel.text = "\(_enterprises.count) resultados encontrados"
            
            if _enterprises.count == 0 {
                noResultLabel.isHidden = false
                resultsView.isHidden = true
            } else {
                noResultLabel.isHidden = true
                resultsView.isHidden = false
            }
            
            self.loadingView.isHidden = true
            
            // TODO: update the tableView
            resultsTableView.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        searchInput.delegate = self
        
        // tableView setup
        resultsTableView.delegate = self
        resultsTableView.dataSource = self
        resultsTableView.register(UINib(nibName: "EnterpriseTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "customCell")
        resultsTableView.separatorColor = .none
        resultsTableView.separatorStyle = .none
        
        // preset views visibilty
        resultsView.isHidden = true
        noResultLabel.isHidden = true
        loadingView.isHidden = true
        
        // Force a first search
        prefixSearch(prefix: "")
        
        // Insert a keyboard movement
        setupKeyboardMoveView()
    }
    
    func repositionBackground() {
        topAreaPosition.constant = 200
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
            self.topImageIcons.alpha = 0
        })
    }
    
    func prefixSearch(prefix: String) {
        self.loadingView.isHidden = false
        self.resultsView.isHidden = true
        self.noResultLabel.isHidden = true
        
        requestManager.search(prefix: prefix) { data, request, error in
            if let data = data {
                let enterprises = try? JSONDecoder().decode(Enterprises.self, from: data)
                if let enterprises = enterprises {
                    self.enterprises = enterprises.enterprises
                }
            }
        }
    }
    
    // MARK: Keyboard fine control region
    func setupKeyboardMoveView() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyBoardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.bottomConstraint.constant = keyboardSize.height
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        self.bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}
