//
//  SearchViewController+UITableView.swift
//  empresas-ios
//
//  Created by Caio Gomes on 21/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import Foundation
import UIKit

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enterprises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") as! EnterpriseTableViewCell
        cell.enterprise = enterprises[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let enterprisePage = CompanyViewController(nibName: "CompanyViewController", bundle: Bundle.main)
        let cell = tableView.cellForRow(at: indexPath) as! EnterpriseTableViewCell
        enterprisePage.enterprise = cell.enterprise
        self.navigationController?.pushViewController(enterprisePage, animated: true)
    }
}
