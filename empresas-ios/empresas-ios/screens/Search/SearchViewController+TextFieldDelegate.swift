//
//  SearchViewController+TextFieldDelegate.swift
//  empresas-ios
//
//  Created by Caio Gomes on 21/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import Foundation
import UIKit

extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        prefixSearch(prefix: textField.text ?? "")
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.repositionBackground()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

