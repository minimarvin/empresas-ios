//
//  MockSearch.swift
//  empresas-ios
//
//  Created by Caio Gomes on 21/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import Foundation

let mockData = "{\"enterprises\":[{\"city\":\"Maule\",\"country\":\"Chile\",\"description\":\"Cold Killer was discovered by chance in the ´90 s and developed by Mrs. Inés Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. \",\"email_enterprise\":null,\"enterprise_name\":\"AQM S.A.\",\"enterprise_type\":{\"enterprise_type_name\":\"Agro\",\"id\":1},\"facebook\":null,\"id\":4,\"linkedin\":null,\"own_enterprise\":false,\"phone\":null,\"photo\":null,\"share_price\":5000.0,\"twitter\":null,\"value\":0}]}"
