//
//  LoginViewController+TextFieldDelegate.swift
//  empresas-ios
//
//  Created by Caio Gomes on 20/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import Foundation
import UIKit

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.repositionBackground()
        self.removeError(textField as! TextField)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

