//
//  LoginViewController.swift
//  empresas-ios
//
//  Created by Caio Gomes on 20/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var welcomeText: UILabel!
    @IBOutlet weak var emailInput: TextField!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordInput: TextField!
    @IBOutlet weak var topBackgroundConstraint: NSLayoutConstraint!
    @IBOutlet weak var errorText: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    var requestManager = RequestManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        
        // TODO: implement a pre-load of the next page if the user is signed in already
    }
    
    
    // TODO: insert a padding between text input and the borders
    func setup() {
        // setup delegate and keyboard reference
        emailInput.delegate = self
        passwordInput.delegate = self
        self.errorText.isHidden = true
        passwordInput.togglePasswordVisibility()
        emailInput.keyboardType = .emailAddress
        loginButton.layer.cornerRadius = 4
        
        // setup the toggle password icon
        setupToggleIcon()
    }
    
    func validateForm() -> Bool {
        var status = true
        
        let email = emailInput.text ?? ""
        let password = passwordInput.text ?? ""
        
        if !isValidEmail(email) {
            makeError(emailInput)
            status = false
        }
        if password == "" {
            makeError(passwordInput)
            status = false
        }
        
        return status
    }
    
    func setupToggleIcon() {
        let toggleButton = UIButton(type: .custom)
        toggleButton.setTitleColor(#colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1), for: .normal)
        toggleButton.setImage(#imageLiteral(resourceName: "eye"), for: .normal)
        toggleButton.addTarget(passwordInput, action: #selector(passwordInput.togglePasswordVisibility), for: .touchUpInside)
        passwordInput.rightView = toggleButton
        passwordInput.rightViewMode = .always
    }
    
    func makeError(_ input: TextField) {
        input.layer.borderColor = #colorLiteral(red: 0.8784313725, green: 0, blue: 0, alpha: 1)
        input.layer.borderWidth = 1
        
        let img = UIImageView(image: #imageLiteral(resourceName: "error-icon"))
        input.rightView = img
        input.rightViewMode = .always
        
        let label = getLabel(forInput: input)
        label?.textColor = #colorLiteral(red: 0.8784313725, green: 0, blue: 0, alpha: 1)
    }
    
    func removeError(_ input: TextField) {
        input.layer.borderWidth = 0
        input.layer.borderColor = .none
        input.rightView = nil
        
        if input == passwordInput {
            setupToggleIcon()
        }
        
        let label = getLabel(forInput: input)
        label?.textColor = .label
    }
    
    func getLabel(forInput input: TextField) -> UILabel? {
        if input == passwordInput {
            return passwordLabel
        } else if input == emailInput {
            return emailLabel
        }
        return nil
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func repositionBackground() {
        self.topBackgroundConstraint.constant = 200
        
        UIView.animate(withDuration: 0.5, animations: {
            self.welcomeText.alpha = 0
            self.view.layoutIfNeeded()
        })
    }

    @IBAction func login(_ sender: Any) {
        let validated = validateForm()
        if validated {
            // TODO: make the request to the API
            self.loadScreen()
            let email = emailInput.text ?? ""
            let password = passwordInput.text ?? ""
            requestManager.signIn(email: email, password: password) { data, response, error in
                self.dismissLoad() {
                    if response?.statusCode == 200 {
                        self.nextPage()
                    } else {
                        // TODO: update credentials with error
                        self.errorText.isHidden = false
                    }
                }
            }
        }
    }
    
    func nextPage() {
        let searchScreen = SearchViewController(nibName: "SearchViewController", bundle: Bundle.main)
        searchScreen.requestManager = self.requestManager
        let navigationController = UINavigationController(rootViewController: searchScreen)
        
        self.view.window?.rootViewController = navigationController
    }
    
}
