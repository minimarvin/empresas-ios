//
//  CompanyViewController.swift
//  empresas-ios
//
//  Created by Caio Gomes on 20/05/20.
//  Copyright © 2020 CaioGomes. All rights reserved.
//

import UIKit

class CompanyViewController: UIViewController {
    
    @IBOutlet private weak var enterpriseLabel: UILabel?
    @IBOutlet private weak var enterpriseImage: UIImageView?
    @IBOutlet private weak var enterpriseBackground: UIView?
    @IBOutlet private weak var contentLabel: UILabel?
    
    private var _enterpriseColor: UIColor = .purple
    var enterpriseColor: UIColor {
        get {
            return _enterpriseColor
        }
        set {
            _enterpriseColor = newValue
            enterpriseBackground?.backgroundColor = _enterpriseColor
        }
    }
    
    private var _enterprise: Enterprise? = nil
    var enterprise: Enterprise? {
        get {
            return _enterprise
        }
        set {
            _enterprise = newValue
            if let enterprise = _enterprise {
                self.enterpriseLabel?.text = enterprise.enterprise_name
                self.title = enterprise.enterprise_name
                // TODO: insert the actual value for logo
//                self.enterpriseImage?.downloaded(from: "https://s2.glbimg.com/z_gIOSUdsxyNGClgVLYVBHBziyw=/0x0:400x400/400x400/s.glbimg.com/po/tt2/f/original/2016/05/20/new-google-favicon-logo.png")
                if let url = enterprise.photo {
                    self.enterpriseImage?.downloaded(from: "https://empresas.ioasys.com.br"+url)
                }
                self.contentLabel?.text = enterprise.description
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        enterpriseBackground?.backgroundColor = _enterpriseColor
        if let enterprise = _enterprise {
            self.enterpriseLabel?.text = enterprise.enterprise_name
            // TODO: insert the actual value for logo
//            self.enterpriseImage?.downloaded(from: "https://s2.glbimg.com/z_gIOSUdsxyNGClgVLYVBHBziyw=/0x0:400x400/400x400/s.glbimg.com/po/tt2/f/original/2016/05/20/new-google-favicon-logo.png")
            if let url = enterprise.photo {
                self.enterpriseImage?.downloaded(from: "https://empresas.ioasys.com.br"+url)
            }
            self.contentLabel?.text = enterprise.description
        }
        
        self.navigationController?.navigationBar.isHidden = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
